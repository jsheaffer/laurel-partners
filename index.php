<?php 

require('classes/LaurelForms.php');

$forms = new LaurelForms;

include_once('templates/header.php');

?>

<p class="intro">Select an event below and navigate to the Parents or Partner forms.</p>
<br/>
<div id="home_settings">
    <div class="options">
        <h3>Forms</h3>
        <br/>
        <?php $forms->pages(); ?>                
    </div>
    <div class="events">
        <h3>Events</h3>
        <br/>
        <?php $forms->events(); ?>
    </div>                
</div>

<?php

include_once('templates/footer.php');	