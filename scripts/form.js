/**
* Form Script
* @author JS
* @version 1.0
*/

$( document ).ready( function() {
    
    const AppRoot = '/laurel_partners/';
    const APIURL = "/laurel_partners/update.php";
    const GRECAPTCHA = {
        key    : "6LczHKgUAAAAAMVaUHyLCTi0R5f61xuaWkQoZ4nV",
        action : "partnership"
    };    
    const SENDING = '<i class="fas fa-circle-notch fa-spin"></i>';
    const FAILURE = '<i class="fas fa-exclamation"></i>';
    const SUCCESS = '<i class="fas fa-check"></i>';
    
    $("a").on( 'click', (event) => {
        event.preventDefault();
        console.log(event.target);
        document.location.href = event.target.href;
    });
    
    if ( window.localStorage.getItem( 'lss_event' ) ) {
        
        $('#eventlist').val( window.localStorage.getItem( 'lss_event' ) );
        $('#event').val( window.localStorage.getItem( 'lss_event' ) );
        
        $('#event_display').html( $("#eventlist").find("option:selected").text() );
    }
    else {
        
        $('#event').val( 1 );
        $('#eventlist').val( 1 );
        window.localStorage.setItem( 'lss_event', 1 );
    }    
    $( '#eventlist' ).on( 'change', function( event ) {
        
        $('#event_display').html( $(event.target).find("option:selected").text() );
        $('#event').val( event.target.value );
        window.localStorage.setItem( 'lss_event', event.target.value );
    });
    
    $('#page').on('change', (event) => {
        
        document.location.href = AppRoot + event.target.value;
    });
    
    $('#settings_button').on('click', (event) => {
        
        $('.menu-drop').toggleClass('show');
    });
    
    // Form Submit Actions
    $( document.partnership ).on( 'submit', function( event ) {
    
        event.preventDefault();
        
        alert( 'sending', SENDING + 'One moment please...');
        
        const form = event.target;

        if ( valid( form ) ) {
            
            grecaptcha.ready( () => {
                grecaptcha.execute( GRECAPTCHA.key, { action: GRECAPTCHA.action })
                    .then( token => check( token ) )
                    .then( human => {
    
                        if ( human ) {
    
                            submit( form );
                        }
                    });
            });
        }
        else {

            alert( 'failure', FAILURE + 'Please fill out all required fields.' );
        }
    });
    
    // Form Reset
    $( document.partnership ).on( 'reset', function( event ) {
        
        $( '.error' ).removeClass('error');
    });
    
    //	Format Phone number on the fly
	$( document.partnership.phone ).on( 'keyup', function( event ) {

		format_phone_number( event );
	});
    
    // Submit Function
    async function submit( form ) {
        
        const data = formatData( form );
        
        console.log( data );
        
        try {
        
            fetch( APIURL, {
                method : 'POST',
                headers : {
                    'Content-Type' : 'application/json'
                },
                body : JSON.stringify( data )
            })
            .then( response => response.json() )
            .then( result => {
                
                console.log(result);
        
                if ( result.success ) {
        
                    $('.btn-clear').trigger('click');
        
                    alert( 'success', SUCCESS + 'Thank you for your submission.' );
                }
                else {
        
                    alert( 'failure', FAILURE + 'Could not process your request at this time.' );
                }
            });
        }
        catch( error ) {
        
            console.log( 'Error: ', error );
            alert( 'failure', FAILURE + 'Could not process your request at this time.' );
        }
    }

    async function check( token ) {
        
        const response = await fetch( APIURL, {
            method : 'POST',
            headers : {
                'Content-Type' : 'application/json'
            },
            body : JSON.stringify({
                recaptcha : token
            })
        });
        
        const result = await response.json();
        
        console.log(result);
        
        if ( result.success && result.score > 0.5 ) {
            
            return true;
        }
        else {
            
            alert( 'failure', FAILURE + 'Could not process your request at this time.' );
            return false;
        }
    }
    
    function valid( form ) {
        
        console.log(form)
        
        $( '.error' ).removeClass('error');
        
        var valid = true;
        
        //	Valid Formats
		var validText   = /^[A-Za-z0-9 -/.]*$/;
		var validEmail  = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
        
        for ( var i = 0; i < form.length; i ++ ) {
            
            var field = form[ i ];
            
            switch ( field.type ) {
                
                case 'text':
                    
                    if ( ( field.value == '' ) || field.value.search( validText ) == -1 ) {

                        highlight( field );
                        valid = false;
                    }
                    break;
                    
                case 'email':
                
                    if ( ( field.value == '' ) || field.value.search( validEmail ) == -1 ) {

                        highlight( field );
                        valid = false;
                    }                    
                    break;
                    
                case 'tel':
                
                    var number = field.value.replace( /\D/g, '' );
                
                    if ( number.length != 10 ) {

                        highlight( field );
                        valid = false;
                    }                
                    break;
                        
                default:
                    
            }
        }
        
        return valid;
    }
    
    function formatData( form ) {
        
        switch (form.type.value) {
            
            case 'parents':
                return {
                    submit : true,
                    record : form.type.value,
                    values : [
                        {
                            fieldId : 'name',
                            value   : [ form.first_name.value, form.last_name.value ].join(' ')
                        },
                        {
                            fieldId : 'companyname',
                            value   : [ form.first_name.value, form.last_name.value ].join(' ')
                        },
                        {
                            fieldId : 'firstname',
                            value   : form.first_name.value
                        },
                        {
                            fieldId : 'lastname',
                            value   : form.last_name.value
                        },
                        {
                            fieldId : 'email',
                            value   : form.email.value
                        },
                        {
                            fieldId : 'phone',
                            value   : form.phone.value.replace( /\D/g, '' )
                        },
                        {
                            fieldId : 'comments',
                            value   : form.comments.value
                        },
                        {
                            fieldId : 'custentity_partnership_event',
                            value   : form.event.value
                        }
                    ]
                };
                break;
            case 'partners':
                return {
                    submit : true,
                    record : form.type.value,
                    values : [
                        {
                            fieldId : 'companyname',
                            value   : form.company_name.value
                        },
                        {
                            fieldId : 'custentitysitecoordinator',
                            value   : [ form.first_name.value, form.last_name.value ].join(' ')
                        },
                        {
                            fieldId : 'email',
                            value   : form.email.value
                        },
                        {
                            fieldId : 'phone',
                            value   : form.phone.value.replace( /\D/g, '' )
                        },
                        {
                            fieldId : 'comments',
                            value   : form.comments.value
                        },
                        {
                            fieldId : 'custentity_partnership_event',
                            value   : form.event.value
                        }
                    ]
                };
                break;                
        }
    }
    
    function alert( status, message ) {
        
        if ( status == 'sending' ) {
            
            $( '#alert' )
                .clearQueue()
                .stop()
                .empty()
                .removeClass()
                .addClass( status )
                .html('<p>{message}</p>'.replace( '{message}', message ) )
                .fadeIn( 600 );
        }
        else {
            
            $( '#alert' )
                .clearQueue()
                .stop()
                .empty()
                .removeClass()
                .addClass( status )
                .html('<p>{message}</p>'.replace( '{message}', message ) )
                .fadeIn( 600 )
                .delay( 3500 )
                .fadeOut( 600 );
        }        
    }
    
    function highlight( field ) {
        
        $( field ).addClass( 'error' );
    }
    
    /**
	 * Format Phone number on the fly
	 * @param {Object} event
	 */
	function format_phone_number( event ) {

		var phone 		= event.currentTarget;
		var phone_input = phone.value.replace( /\D/g,'' );
		var ignore_keys = [ 8, 46 ];
		var cursorPos   = phone.selectionStart;

		if ( ignore_keys.indexOf( event.keyCode ) !== -1 ) {

			return;
		}

		if ( phone.textLength >= 1 ) {

			phone.value = phone_input;
		}
		if ( phone_input.length >= 3 ) {

			var display_phone = '(' + phone_input.slice( 0, 3 ) + ') ' + phone_input.slice( 3, 6 );
		}
		if ( phone_input.length >= 6 ) {

			display_phone =  display_phone + ' - ' + phone_input.slice( 6, 10 );
		}
		if ( phone_input.length >= 11 ) {

			display_phone =  phone_input.slice( 0, 1 ) + ' - ' + phone_input.slice( 1, 4 ) + ' - ' + phone_input.slice( 4, 7 ) + ' - ' + phone_input.slice( 7, 11 );
		}
		if ( phone_input.length >= 12 ) {

			display_phone =  phone_input.slice( 0, 2 ) + ' - ' + phone_input.slice( 2, 5 ) + ' - ' + phone_input.slice( 5, 8 ) + ' - ' + phone_input.slice( 8, 12 );
		}
		if ( phone_input.length >= 13 ) {

			display_phone =  phone_input.slice( 0, 3 ) + ' - ' + phone_input.slice( 3, 6 ) + ' - ' + phone_input.slice( 6, 9 ) + ' - ' + phone_input.slice( 9, 13 );
		}

		if ( display_phone ) {

			phone.value = display_phone;

			if ( cursorPos < phone_input.length ) {

				event.currentTarget.setSelectionRange( cursorPos, cursorPos );
			}
		}
	}
    
});
