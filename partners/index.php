<?php

require('../classes/LaurelForms.php');

$forms = new LaurelForms;
$forms->page = 'partners';

include_once('../templates/header.php');

$forms->intro();

?>

<!-- Partnership Form -->
<form id="partnership" name="partnership" class="lss-forms" enctype="multipart/form-data" autocomplete="off">
    
    <h2 class="title">Potential Partners</h2>
    
    <?php 
        $forms->field( array(
            "company_name" => "text",
        ));
    ?>
    <span class="half">
        <?php 
            $forms->field( array(
                "first_name" => "text",
                "last_name"  => "text"
            ));
        ?>
    </span>
    <span class="half">
        <?php 
            $forms->field( array(
                "email" => "email",
                "phone" => "tel"
            ));
        ?>
    </span>
    <?php 
        $forms->field( array(
            "comments" => "textarea",
            "type"     => "hidden",
            "event"    => "hidden"
        ));
    ?>
    <!-- Buttons -->
    <div class="buttons">
        <!-- Submit -->
        <input type="submit" name="submit" form="partnership" value="Submit" class="btn btn-enter" />
        <!-- Reset -->
        <input type="reset" name="reset" form="partnership" value="Reset" class="btn btn-clear" />
    </div>
</form>

<?php

include_once('../templates/footer.php');