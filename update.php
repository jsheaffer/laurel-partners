<?php

require( './classes/NetSuiteConnection.php' );

$data = json_decode( file_get_contents('php://input') );

if ( isset( $data->submit ) ) {

    $record   = new NetSuiteConnection;
    $response = $record->add( $data );
}

if ( isset( $data->recaptcha ) ) {
    
    echo file_get_contents( sprintf( 
        "%s?secret=%s&response=%s", 
        "https://www.google.com/recaptcha/api/siteverify", 
        "6LczHKgUAAAAAFx2Oxxc24IsrQuRYR927XHXOyzf", 
        $data->recaptcha
    ));
}