<?php


class LaurelForms {
    
    private $ns;
    public $page;
    
    public function __construct() {
        
        require('NetSuiteConnection.php');
        
        $this->ns = new NetSuiteConnection;
    }
    
    
    public function intro() {
        ?>
        
        <div class="intro">
            <p>While successful online learning depends on sophisticated technology, it’s really all about people. Effective distance learning depends on a cohesive educational team: site coordinators committed to playing a strong supportive role, dedicated teachers who are experts in online learning, and students who are motivated to develop effective learning and time management skills.</p>
        </div>
        
        <?php
    }
    
    public function settings() {
        
        if ( empty( $this->page ) ) {
            return;
        }
        
        ?>
        
        <div id="settings">
            <div id="settings_display">
                <span id="event_display">Select an Event</span>
                <div id="settings_button" class="square menu-drop">
                    <i class="fas fa-sliders-h fa-lg menu-icon"></i>
                </div>                
            </div>
            <div id="settings_menu" class="menu-drop">
                <div class="options">
                    <h3>Forms</h3>
                    <br/>
                    <?php $this->pages(); ?>                
                </div>
                <div class="events">
                    <h3>Events</h3>
                    <br/>
                    <?php $this->events(); ?>
                </div>                
            </div>
        </div>        
        
        <?php
    }
    
    public function field( $args ) {
        
        if ( empty( $args ) ) {
            return;
        }
        
        foreach( $args as $name => $type ) {
            
            switch ( $type ) {
                case 'text':
                case 'email':
                case 'tel':
                
                    echo sprintf(
                        '<input form="partnership"
                                type="%s" 
                                name="%s" 
                                placeholder="%s" 
                                value="" />',
                        $type,
                        $name,
                        ucwords(str_replace('_', ' ', $name))
                    );
                    
                    break;
                case 'hidden':
                        
                    $value = ($name == 'type') ? $this->page : '';
                    $id    = ($name == 'event') ? $name : '';
                
                    echo sprintf(
                        '<input form="partnership"
                                type="%s" 
                                name="%s" 
                                placeholder="%s" 
                                value="%s" 
                                id="%s"  />',
                        $type,
                        $name,
                        ucwords(str_replace('_', ' ', $name)),
                        $value,
                        $id
                    );
                        
                    break;
                case 'textarea':
                    
                    echo sprintf(
                        '<textarea form="partnership"
                                   name="%s" 
                                   placeholder="%s" 
                                   value=""></textarea>',
                        $name,
                        ucwords(str_replace('_', ' ', $name))
                    );
                    
                    break;
                default:
                    // code...
                    break;
            }
        }
    }
    
    public function events() {
        
        $response = $this->ns->get();
        $events   = json_decode( $response );
        
        echo '<select id="eventlist" name="events">';
        
        foreach ( $events as $event ) {
            
            echo sprintf(
                "<option value=%d>%s</option>", 
                $event->id,
                $event->name
            );
        }
            
        echo '</select>';
    }
    
    public function pages() {
        
        $page_list = array( 'parents', 'partners' );
        
        echo '<ul id="pagelist">';
        
        foreach( $page_list as $page ) {
            
            $active = ($this->page == $page) ? 'active' : '';
            $href   = "/laurel_partners/".$page."/";
            
            echo sprintf(
                '<li class="%s"><button onclick=javascript:window.location.href="%s">%s</button></li>',
                $active,
                $href,
                ucfirst($page)
            );
        }
            
        echo '</ul>';
    }
    
    public function select( $type ) {
        
        $options = array(
			'grade' => array(
				''  => 'Grade Needed',
				1   => 'Kindergarten',
				2   => '1st Grade',
				3   => '2nd Grade',
				4   => '3rd Grade',
				5   => '4th Grade',
				6   => '5th Grade',
				7   => '6th Grade',
				8   => '7th Grade',
				9   => '8th Grade',
				10  => '9th Grade',
				11  => '10th Grade',
				12  => '11th Grade',
				13  => '12th Grade',
			)
        );
        
        foreach ( $options[$type] as $key => $value ) {
            
            echo sprintf(
                '<option value="%d">%s</option>',
                $key,
                $value
            );
		}
    }
    
    public function alert() {
        
        echo '<div id="alert"></div>';
    }
    
    
} // ...End Class