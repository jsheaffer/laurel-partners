<?php 

require_once('OAuth.php');
require_once('NetSuiteSettings.php');

class NetSuiteConnection extends NetSuiteSettings {
    
    private $token;
    private $consumer;
    private $signature;
    private $context;
    
	function __construct() {
        
        $this->token     = new OAuthToken( $this->token_id, $this->token_secret );
        $this->consumer  = new OAuthConsumer( $this->consumer_key, $this->consumer_secret );
        $this->signature = new OAuthSignatureMethod_HMAC_SHA1();
    }
    
    public function add( $args = array() ) {
        
        $this->context = $this->get_context( 'POST', $args );
        
		try {
        
			echo file_get_contents( $this->href, false, $this->context );
		}
		catch ( Exception $error ) {
        
			return $error;
		}
    }
    
    public function get( $args = array() ) {
        
        $this->context = $this->get_context( 'GET', $args );
        
		try {
        
			return file_get_contents( $this->href, false, $this->context );
		}
		catch ( Exception $error ) {
        
			return $error;
		}
    }
    
    public function get_context( $method, $args ) {

		$data = json_encode( $args );
        
        $request = new OAuthRequest( $method, $this->href, array(
            'oauth_nonce'            => $this->generateNonce(),
    		'oauth_timestamp'        => time(),
    		'oauth_version'          => '1.0',
    		'oauth_token'            => $this->token_id,
    		'oauth_consumer_key'     => $this->consumer_key,
    		'oauth_signature_method' => $this->signature->get_name()
        ));
        
        $request->set_parameter( 'oauth_signature', $request->build_signature(
            $this->signature,
            $this->consumer,
            $this->token
        ));
        
        $request->set_parameter( 'realm', $this->account );

		$options = array(
			'http' => array(
	      	    'header'  => $this->get_header_string( $request ),
				'method'  => $method,
				'content' => $data
			)
		);
        
		return stream_context_create( $options );
	}
    
    public function get_header_string( $request ) {

		$auth_string = implode( ',', array(
            $request->to_header(),
            'realm="' . $this->account . '"'
        ));
        
        $string = $auth_string;
        $string = $string . "\r\n" . sprintf( "Host: %s", $this->host);
		$string = $string . "\r\n" . "Content-Type: application/json";

		return $string;
	}
    
    public function generateNonce() {
        
        $length           = 20;
        $characters       = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString     = '';
        
        for ( $i = 0; $i < $length; $i++ ) {
            
            $randomString .= $characters[random_int(0, $charactersLength - 1)]; 
        }
        
        return $randomString;
    }
}