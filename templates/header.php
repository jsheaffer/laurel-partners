<?php 

// Laurel Partnership App

?>

<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0;">
	<meta name="apple-mobile-web-app-capable" content="yes">

<title>Laurel Springs School Partnerships</title>

<!-- Favicons -->
	<link href="/laurel_partners/images/favicon.png" rel="icon" type="image/ico" />
	<link href="/laurel_partners/images/favicon.png" rel="apple-touch-icon" />
   	<link href="/laurel_partners/images/favicon.png" rel="apple-touch-icon-precomposed"/>
<!-- Styles -->
	<link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Rufina:400,700|Sintony:400,700&display=swap' >
	<link href="/laurel_partners/styles/main.css" rel="stylesheet" type="text/css" />

</head>
<body>
    
    <header>
    <div class="wrapper">
            
		<div class="logo">
			<img src="/laurel_partners/images/logo.png" alt="Laurel Springs School">
        </div>
		
		<?php $forms->settings(); ?>
			
    </div>
    </header>
	
	<div class="main">
	<div class="wrapper">