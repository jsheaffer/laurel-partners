</div>

<?php $forms->alert(); ?>

</div>

<footer>
    <div class="footer-info">
        <p>Laurel Springs School</p>
        <p>1615 West Chester Pike West Chester PA 19382</p>
    </div>
</footer>


<!-- Vendor -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="/laurel_partners/scripts/vendor/placeholders.min.js"></script>
<script src="/laurel_partners/scripts/vendor/modernizr.js"></script>
<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script> 
<script src="https://www.google.com/recaptcha/api.js?render=6LczHKgUAAAAAMVaUHyLCTi0R5f61xuaWkQoZ4nV"></script>

<!-- Local -->
<script src="/laurel_partners/scripts/form.js"></script>

</body>
</html>
